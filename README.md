# Who are you?

@hckrtst

# What the heck is this?

The project is expected to be a comprehensive collection of topics
similar to those typically found in tech interviews?

# How do I join this group?

Send a pull request!

> Remember to add your name to this file

# What language can I use to solve the problems?

Any language you like. If you do use an obscure language then use plenty of comments.
